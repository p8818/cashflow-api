const express = require('express');
const app = express();
const hash = require('string-hash');
const moment = require('moment');
const mysql = require('mysql');
const config = require('../config')
const { createConnection } = require('mysql');
const dotenv = require('dotenv');
dotenv.config();

var connection = mysql.createPool(config.connection);

// GET users
exports.getSubscriptions = (req, res) => {
    var sql = `SELECT * FROM users`;
    connection.query(sql, (err, result) => {
         if (err) throw err;
         res.status(201).json(result);
    });
}